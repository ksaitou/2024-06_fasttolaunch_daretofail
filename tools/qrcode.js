const path = require("path");
const qrcode = require("qrcode");

const projectdir = (...paths) => path.join(__dirname, "../", ...paths);

qrcode.toFile(
  projectdir("slides/images/qrcode.png"),
  "https://ksaitou.gitlab.io/2024-06_fasttolaunch_daretofail/"
);

// qrcode.toFile(
//   projectdir("slides/images/totp_qrcode.png"),
//   "otpauth://totp/YourAccountName:user@example.com?secret=BASE64ENCODEDSTR&issuer=MyGreatWebService&period=30"
// );
