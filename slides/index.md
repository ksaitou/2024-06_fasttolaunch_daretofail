---
title: 2024-09 「何度でもサービスを作ろう」
marp: true
paginate: true
markdown.marp.enableHtml: true
style: |
  section > header {
    top: 0.05rem;
  }
  section > footer {
    bottom: 0.05rem;
  }
  section::after {
    bottom: 0.05rem;
  }

  section p, section li {
    font-size: 0.8rem;
  }

  section.primary_section {
    background-image:
      linear-gradient(rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.3)), 
      url("images/logo.png");
    background-position: right bottom;
    background-repeat: no-repeat;
  }

  section.primary_section h1 {
    font-size: 3.2rem;
    text-shadow: #FFF 0px 0 10px;
  }
  section.primary_section p {
    color: #999;
  }

  secrtion.secondary_section {
    background-color: #ccc;
  }
  section.secondary_section h1 {
    font-size: 2rem;
    text-align: center;
  }

  section > h2 {
    margin-bottom: 0;
  }

  section::after {
    content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
  }

  section > ul > li {
    line-height: 1.35;
    & li {
      line-height: 1.22;
    }
  }

  img[alt="uml diagram"] {
    flex: 1;
  }

  span.sub {
    display: block;
    color: #999;
    font-size: 80%;
    line-height: 1;
  }

  .page_logo {
    display: block;
    position: absolute;
    top: 10px;
    right: 10px;
  }

  img.page_logo, .page_logo img {
    width: 100px;
    margin-left: 0.25rem;
  }
---

<!-- _class: primary_section -->

# 2024-09 何度でもサービスを作ろう

2024/09 by Kenji Saitou

![Slides are here](images/qrcode.png)

- スライド: https://ksaitou.gitlab.io/2024-06_fasttolaunch_daretofail/
- リポジトリ: https://gitlab.com/ksaitou/2024-06_fasttolaunch_daretofail/

<!-- mermaid.js -->
<script src="https://unpkg.com/mermaid@10.8.0/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

---

<!--
header: "今日話すこと"
-->

## 今日話すこと

**技術チーム**側から見た**Web サービスの立ち上げ〜運用・運営に関わる幅広いフルスタックな話**をしていきます。

- 技術編 〜 サービスを**立ち上げる**
  - どうやってサービスを立ち上げればいいのか？
  - ローコストで最強サービスを立ち上げられるか？
- 運用編 〜 サービスを**運用する**
  - サービスの保守と発展を考える
    - 請求、監視、ログ、アラート などなど
- 運営編 〜 いろんな人達からの**期待に応える**
  - 社内からの期待
  - 社外（取引先）からの期待
- サービスの**終了** 😔
- 本日の**まとめ**

主に**ここ数年での部のサービス開発体験がベース**です。

---

## 免責事項

- 会社のコンプライアンス遵守上、適用できない可能性のある技術要素を取り上げます
- 技術チームから見たサービス立ち上げの話をします
  - 経理的な話はカバーしません（作ったサービスの償却など）
  - 企画・営業的な話もあまりカバーしません
- 特にサービスの運用基準について強い見解を示すものではありません
  - 取り入れられるところは持ち帰ってください
- 本日も文字多め＆話題多めです
  - ついていけない人は全体的な話を拾ってください
- 自分たちのことは**極力棚に上げて話します**
- 本スライド作成にあたり、同じ部の方にインタビュー協力いただきました。ありがとうございました。

**では、スタート！**

---

<!--
_class: primary_section
header: "技術編 〜 サービスを立ち上げる"
footer: ""
-->

# 技術編 〜 <br>サービスを立ち上げる

サービスの立ち上げ方、どんなものを用意するかなどについて見ていきます

---

<!--
footer: "サービスをとりあえず立ち上げよう"
-->

## サービスを立ち上げよう 〜 どういうきっかけがあるか？

- **全く新しいサービス**の立ち上げ
  - 「XXX をする Web サービスがあったらいいな」
    - この時点ではサービス名もロゴも何もない
    - 当たる？外れる？ とにかく立ち上げないと分からない
  - 実証のために**とにかくサービスをすぐに立ち上げたい**
    - **うまくいったら、そのうち大きく成長するかも！**
- 既存サービスに付随する **（準）サービス** の立ち上げ
  - サポートサイト・アンケートフォーム・請求管理・マイクロサービス etc.
  - 不便さ、作業ミス多発を解消できればいい
  - **可用性は 24H7D MAX じゃなくてもいいことも多い**
  - なぜ本体に組み込まないのか？
    - 本体はリリース手順が重たいし、壊れると大変
    - 最悪、**付随サービスが壊れても文句をいう人は少ない**
    - 負荷は分かれるに限る
- **部内サービス**の立ち上げ
  - 「部内で便利に使える XXX があればいいな」
  - **いくらでも部内調整可能**

---

## とりあえず立ち上げないと進まない 〜 MVP を考えよう

- [MVP](https://ja.wikipedia.org/wiki/%E5%AE%9F%E7%94%A8%E6%9C%80%E5%B0%8F%E9%99%90%E3%81%AE%E8%A3%BD%E5%93%81) (Minimum Viable Product: **一番小さな完成品**) を考えよう
- Web サービスとして **最低限のみを作って** リリースしていく
  - とりあえず **最初はログインだけできればいい** と思います
  - 機能をブレイクダウンして **段階的に作る**
    - 例えば、データ削除機能や管理機能は初期段階では無くても成り立つ
    - 最悪データベースを直接編集して管理機能とすることもできる（手順書は必要）
- そのサービスが**何をするか・何をしないかコンセプトは明確にする**
- まず何でもいいから **初手で動いているものを関係者向けに出す**
  - 動いているものがないと誰も検証できない、脳が納得しない
- 世の中に出すと、そこから終わりなき運用・保守が始まることは覚悟する
  - （会社・社会の）**"当たり前品質" を満たさないものは世の中に出せない**
  - **一度出したらサービスの互換性を破壊する変更は基本的にできない** (ちゃぶ台返し禁止)
    - だからこそ「やること」「やらないこと」コンセプトは固めたほうがいい
- 当たるかも分からない **サービス初期でコストフルな構成は選べない**
  - 安＆早＆楽から、**サービスの成長にあわせて徐々に正統進化**していける構成を選びたい

---

<!--
footer: "Webサービスを成立させる最低限の部品"
-->

## Web サービスを成立させる最低限の 3 部品

- 目安として、この 3 つがあれば Web サービスとして一通りの処理はできる
  - **データベース・データストア**
  - **画面(＆API) (同期処理)**
  - **バッチ (非同期処理)**

```mermaid
sequenceDiagram
  participant User as ユーザ
  participant Web as 画面/API
  participant Batch as バッチ
  participant DB as データベース<br>データストア

  alt リアルタイムで処理するもの
    User->>Web: 画面からの操作（同期）
    Web->>DB: データを取得・変更・バッチジョブの登録
  else バッチジョブ
    Batch->>DB: ジョブの実行（非同期）
  end
```

---

## Web サービスを成立させる最低限の 3 部品

- 【重要度 **高**】 **データベース・データストア**
  - **アプリの中核**になる。ここを起点に考えてもいい。アプリより寿命が長い。
    - 信頼性や移植性が重要。自作ダメゼッタイ。
  - サイズ**小**・変更**多**・加工**大**・大事なデータ - **RDBMS** 〜 PostgreSQL, MySQL, sqlite3
  - サイズ**大**・変更**少**・加工**無**・画像など - **オブジェクトストア** 〜 Amazon S3, Azure Blob Storage
  - サイズ**小**・変更**多**・加工**少**・高速 - **インメモリ DB・NoSQL** 〜 Redis, DynamoDB
  - DB と画面だけでシステムを完結させたい - **mBaaS** 〜 Supabase, Firestore
- 【重要度 中】 **画面(＆API) (同期処理)**
  - **人（外部システム）がシステムをオンラインで参照・指示する** ために作る。基本的に自作。
    - Web サーバへのリクエストとレスポンス（応答時間 1 秒未満程度目安）
  - Web 画面、スマホアプリ、それらが対話する API など
- 【重要度 低】 **バッチ (非同期処理)**
  - オンラインで処理できなかったものを**裏で片付ける**ために作る。基本的に自作。
    - データクリーンアップ、集計、通知、など（深夜の処理が多い）
  - MVP では考えなくていいことも多い

---

## 他に Web サービスに必要なものは？

- **権威 DNS サーバ**
  - ドメイン取得(`yourservice.example.com`)とそのゾーンの維持・管理
  - **権威 DNS サーバは半端な覚悟で自前で立てない** （自分で立てる意味はゼロ）
  - ドメインを手放す際は跡地を他人に悪用されないように計画的に
  - 例: [Amazon Route 53](https://aws.amazon.com/jp/route53/), [Google Cloud Domains](https://cloud.google.com/domains/docs/register-domain?hl=ja), [Azure DNS](https://azure.microsoft.com/ja-jp/products/dns), [Cloudflare DNS](https://www.cloudflare.com/ja-jp/application-services/products/dns/) (個人開発者には人気)
- **電子メール配信サービス** (EaaS)
  - メールを送信したり受信したりそのログを見るためのサービス
    - WebAPI・SMTP でちゃんとしたメールが送れる・送信元認証(SPF/DKIM)の設定が簡単
  - **SMTP サーバは半端な覚悟で自前で立てない** (99%のサービスは自前で立てるべきじゃない)
    - よほど大量送信(?!)するとかであれば自前で立ててもいいかも(?!)
  - 例: [SendGrid](https://sendgrid.com/en-us), [Amazon SES](https://aws.amazon.com/jp/ses/), [Mailgun](https://www.mailgun.com/), [MailTrap](https://mailtrap.io/ja/)
  - 全部知っておいて損はない: 評判(IP レピュテーション)・受信拒否(ソフトバウンス・ハードバウンス)・送信元認証(SPF/DKIM/DMARC)・購読解除
    - B2B 契約ユーザー以外への不特定多数への大量配信を前提に入れると途端に難度が上がる
  - 固定 IP で評判を育てるのに苦労する
    - たまに固定 IP からのメール送信を希望する顧客がいる
- ロードバランサー/CDN/プッシュ通知/SMS 通知 など

---

<!--
footer: "そのサービス、どこで動かす？ プロダクトは何を選ぶ？"
-->

## そのサービス、どこで動かす？

- **今や選択肢は無限**
  - 大手パブリッククラウド
    - AWS (Amazon), GCP (Google), Azure (Microsoft), OCI (Oracle)
  - 小規模クラウド・mBaaS・CDN
    - [Heroku](https://jp.heroku.com/), [Netlify](https://www.netlify.com/), [Vercel](https://vercel.com/)
    - Supabase, Firebase
    - [Cloudflare](https://www.cloudflare.com/ja-jp/)
  - などなどあるが、あまり色々な会社を使って闇鍋になりたくない
    - しかし最終的に複数社のミックス構成になるのは避けられないかな……
- **費用感が知りたい**
  - ユーザ数 0 〜 100 〜 10,000 の時のインフラ月額費用を軽く見積もっておきたい
    - どうせリアルに請求を受ける額が一番正しいので、**見積を正確にしようと頑張りすぎない**
    - 新規システムならユーザ 0 から始めて総合で 10 万円〜は一つの目安
      - 構成によっては 1 万円以内も出来るは出来る
        - ローコスト設計は個人開発者の領域に入ってくる
  - 当たってもないサービスでいきなり大きい構成を始めない
    - 当たった時に**順当進化できるような構成を選ぶ**のは意識する

---

## 動かす場所の考え方 〜 DB 駆動で考える

- だいたいどのシステムでも **DB が一番コストを必要としている**
  - → それなら DB を中心にサービスを動かす場所を考えてみる
  - 信頼性が必要＆アプリ自体より高い可用性が必要
  - 性能のボトルネックの原因になりやすい
- **大手パブリッククラウドの RDBMS (OSS・OSS 互換)**
  - AWS: RDS, Aurora Serverless v2 など
  - Azure: Azure Database for XXX Flexible Server など
  - Google Cloud: [Cloud SQL](https://cloud.google.com/sql?hl=ja), [Spanner](https://cloud.google.com/spanner?hl=ja) など
  - **素朴な無難系＆尖った分散ハイスペ or サーバレス系の二本立てが多い**
  - DB に**サーバレス**を望むか？
    - DB を動かすサーバの管理は不要 ⇔ サービスが閑古鳥なら安いが、料金体系は複雑な傾向
    - スパイク（TVCM などでの急な来客増）を想定するか？
  - RDBMS は便利な半絵 m ん性能の頭打ちが問題になることが多い
    - DB は書き込みの多さに対しては大きくスケールしないが、読み取りの多さなら無数にスケールする
    - アプリケーションの読み取り操作(`GET`リクエスト)のみ読み取り専用のレプリカから読み取る

---

## 動かす場所の考え方 〜 DB 駆動で考える

- **NoSQL**
  - 例: AWS DynamoDB, Azure Cosmos DB, Google Cloud Firestore, MongoDB
  - NoSQL: **RDB じゃない新興系 DB の総称**
    - 一方、RDB っぽくインタフェースを寄せた新興系の DB は NewSQL という
  - いいところ
    - API 型でコネクションレスで処理できる
    - スケールアウトの限界を考えなくていい
    - 安く済むケースも多い
  - **設計が難しい**
    - どう転ぶか分からないシステムに組み込まないほうがいい(個人の感想)
    - **JOIN など無く読み込み・加工機能に大きな制約がある**ので、RDBMS でさんざん訓練してきた事実（ファクト）の正しい収集（正規化など）より、**どうやって画面から値を読み取るかまでデータ設計に組み込む必要がある**

---

## 動かす場所の考え方 〜 DB 駆動で考える

- **尖りまくりの互換系 DBaaS / NewSQL**
  - PostgreSQL: [Supabase](https://supabase.com/), [Cockroach DB](https://www.cockroachlabs.com/), [YugabyteDB](https://www.yugabyte.com/jp/), [Neon](https://neon.tech/)
  - MySQL: [PlanetScale](https://planetscale.com/), [TiDB](https://pingcap.co.jp/tidb/)
  - SQLite: [Cloudflare D1](https://developers.cloudflare.com/d1/)
  - 通常の RDB には無いグローバルレベルな高可用性と耐障害性や面白機能
    - データベースの即時コピー（本番から即時にテスト用 DB 作るなど）
      - クローンへの論理レプリケート
    - ロック無し（無停止）スキーマ変更
    - パッチやメンテナンスによるダウンタイム無し
  - AP<=>DB 間の RTT(通信の往復時間)や通信経路が問題
    - 高 RTT の場合は大量の SQL クエリを発行すると通信時間だけで遅くなる
    - パブリッククラウド内に立てられる or インターネットを通じてアクセスする必要がある
    - そもそも DB ネイティブの TCP 接続ではなく、似たような HTTP の API につなぐ必要（専用ドライバが必要）だったり

---

## 動かす場所の考え方 〜 プロダクト・サービス・アーキテクチャの除外基準を考える

- 基本的な**機能や要求特性を満たさないものを除外**
  - 絶対に達成したい機能性・非機能性(暗号化など)・シナリオに沿わないものを除外
- **日本国内で提供していない**サービスを除外 (2024-09 現在)
  - (DBaaS) 🙆 OK: TiDB, Supabase, PlanetScale / 👎 NG: Cockroach DB
  - これに限らずセキュリティとコンプライアンス基準を満たせないサービスは除外
- **互換性**で除外
  - (DB) PostgreSQL or MySQL と互換ではないデータベース
  - ローカルに閉じる開発環境を立ち上げにくい技術は除外
- **凄そうだけど結局難しそうなもの** （身の丈にあわないもの）は除外
  - No/NewSQL, マルチマスター, グローバル分散データベース, Kubernetes, イベントソーシング etc.
- **企業活動としてペイできないもの**は除外
  - 手間がかかりすぎる、筋はいいが英語しかドキュメントがなく引き継ぎ相手が見つからなさそう
  - AWS 中心でやってきたのに別プラットフォームに手を出す
- サポートがないものは除外
- **業界最適とチーム最適は違う**

---

## 動かす場所の考え方 〜 その他

- **プロダクト選定で星取表はやめろ**
  - 他人の認知を狂わせる（誘導する）手段としては有効
    - 対数スケールで違う重要度が同じように並んでいませんか？
  - 主体的にチームで開発リスクを取る場合には正直イマイチだと思う
  - 決め手がないケースに仕方なく補助的な指標として使うならアリ
- **日本代理店に気をつけろ**
  - 米国企業から提供されているサービス、実際の契約相手はそこじゃなかったりする
    - **日本や各国に排他的な代理店を設定している場合がある**（実質そこと契約するしかない）
  - 本国の直接サポートも受けられず、代理店に一次受付してもらうことになる
  - こういうケースではサイトから直接申し込みすれば申し込み完了ではなく、日本の代理店にまず連絡を取らないといけない点に注意
  - `🔍️ プロダクト名` で検索して関係なさそうな**日本企業名がトップでヒットする場合は、そこが排他的な代理店である可能性が大です**

---

## 動かす場所の考え方 〜 フロントエンド（画面）

- **画面の組み方によって選択肢が変わる**: MPA or SPA(CSR or SSR) [※ 去年の発表](https://ksaitou.gitlab.io/2023-10_postmodernfrontend/)
  - **うまくやればサーバ不要でいける**
- **MPA** (Multi Page Application)
  - サイトの HTML (DOM) を主にサーバで生成してクライアントに返す
    - PHP や Rails など**従来型の Web アプリはこれ**
  - **動作するための AP サーバの確保が必要**
- **SPA** (Single Page Application)
  - **CSR** (Client-Side Rendering)
    - サイトの HTML (DOM) をクライアントの JavaScript で全てレンダリングする
      - 画面以外の処理は全て JavaScript から API 通信して行う
      - どのみち外部連携やモバイルアプリからの通信で API サーバ自体は必要
    - **画面自体は静的コンテンツ配信で配信でき、サーバを不要にすることもできる**
  - **SSR** (Server-Side Rendering)
    - CSR についてクライアント任せにせず一部サーバ上でレンダリングを行い UX を改善する
    - **動作するための AP サーバの確保が必要**
- ユーザの行動状態（セッション）を入れるセッションストレージはどこに？
  - SPA なら全てブラウザ上に保存するのが基本
  - そうじゃない場合は**セッションストレージ（Redis などのサーバ）を確保する必要がある**

---

## 動かす場所の考え方 〜 バックエンド（AP・API・バッチ）

- **サーバ**: IaaS (Infrastructure - Amazon EC2, Lightsail, Azure Virtual Machines 等)
  - OS の管理って結構面倒臭い
    - 定期的なパッチ当て＆OS の EOL に合わせたアップグレード（相当苦労する）＆その他色々
  - DB 等データストアをマネージドサービスとして分離してきちんと外付けする形にできているなら、サーバを毎回捨てたりできるので、そういう逃げ方もアリ（実質サーバレスに近い）
  - サーバが事故で落ちた場合の可用性の管理も自分で担保することになるのが面倒
- **サーバレス**: OS 以下の管理をクラウドに丸投げ
  - **FaaS** (Function - AWS Lambda, Azure Functions, Google Cloud Functions 等)
    - こちらが用意した関数をクラウド提供のテンプレート環境で動かす
    - 勝手にスケールアウトしてくれるが、制約が多い
      - 制約: 最大実行時間・リクエスト最大サイズ・起動遅い・DB 接続が微妙 etc.
      - DB についてはコネクション維持が必要な RDBMS は諦めて DBaaS や NoSQL 系(DynamoDB)にすることが多いかも
    - 価格が安いことが多い
    - どちらかというと DB が無いケース（他サービスへの中継）に向いている
  - **CaaS** (Container - AWS ECS＆Fargate, Azure App Service, Google Cloud Run 等)
    - こちらが用意した Docker (OCI) イメージをクラウドが用意したセキュア OS 上で動かす
      - OS を持たないが、FaaS よりはるかにリアルマシンに近い感覚で動かせる
    - 価格は安くない

---

## 動かす場所の考え方 〜 バックエンド（AP・API・バッチ）

- 第 3 の選択肢 〜 **エッジコンピューティング**
  - CDN サービスが FaaS をエッジ（CDN が保持するエンドユーザに近いデータセンター）上で提供している
  - **ユーザの近くで関数が実行されるため低遅延で高速な応答が可能**
  - [Cloudflare Workers](https://www.cloudflare.com/ja-jp/developer-platform/workers/) (Pages), AWS Lambda Edge
    - 実質 Cloudlare の独擅場かな
  - ただし、制約が多い（FaaS と同じく）
- 日本国内だけなら別に CDN とか言わなくても十分速くない？でも使う理由は？
  - キャッシュの制御
  - オリジンの隠蔽、DDoS からの保護
  - IPv6 への対応

---

## 動かす場所の考え方 〜 中身の技術スタックはどうする？

- **どこにどうやってデプロイするかによって技術スタック(言語の選定など)選択肢が変わる**
  - 例えば、FaaS では DB のコネクションプールは使えない
  - 最近は Docker のおかげで Docker に載せればある程度中身は問わないケースも多い
    - **[The Twelve-Factor App](https://12factor.net/ja/) を意識することは超大前提**なので注意
- **ベンダーロックインしたくない**
  - デプロイを Docker に寄せるだけでも割と選択肢は広がると思う
  - 性能重視の重火器は必要に応じて後で投下できればいい
- 個人的には基本的には以下のような考えですが、色々あっていいと思います
  - 画面(SPA/CSR): TypeScript, [React](https://ja.react.dev/) および [Remix](https://remix.run/) などフレームワーク + Docker
    - 結局フレームワーク戦争で React が勝利している
  - API: Go, GIN などのサーバフレームワーク + Docker
    - Go は素の状態で [C10K 問題](https://ja.wikipedia.org/wiki/C10K%E5%95%8F%E9%A1%8C) を解決している＆起動がとても早い
  - DB: PostgreSQL ベースの何か
  - (必要であれば) インメモリ DB: [Redis](https://redis.io/) ([Valkey](https://valkey.io/))
  - オブジェクトストレージ: S3 もしくは S3 互換の何か

---

## 私のつまらない結論 〜 AWS or VM

- 結局、**AWS あたりで RDS をベースにコンテナでシステム組むのが無難といえば無難**
  - 国内リージョンに閉じることができて、コンプライアンスもはっきりしていて、難しい技術もなく、暗号化(TDE)もあるし、冗長性(MultiAZ)もあり、バックアップも用意で、PITR(任意時刻の DB 復元)もあり、ネットワーク限定性(VPC)もあり、メンテナンスパッチも代行され、スケールアップ・アウトも用意で、あらゆるドキュメントと CLI(API)とコンパニオンサービスが用意され……
    - **何か文句あるか？**
    - AWS は外部データ送信(egress)の料金が高いことはよく批判されている
  - **最安値は狙えないがトータルコスト的には安い**とも言える
  - 閑古鳥の最低維持費 ≒ 2.5 万/環境/月 (CloudFront => ALB => 2\*Fargate => 2\*RDS(MultiAZ))
    - ステージング環境・本番環境で 2 環境〜 値段は見る
- ただ、やっぱりちゃんと AWS をやると構成が面倒＆ややコストを食うので、**妥協できるシステムは 1 台の VM 立ててその中に雑に全部入れる**
  - ローカルホスト同士の通信なので DB との通信遅延が最小というメリットもある
  - サービスコンポーネント自体は Docker コンテナとして作成しておく
  - うちでは [Amazon Lightsail](https://aws.amazon.com/jp/lightsail/) でやってます
    - そこまで重要じゃないサービスや開発サーバに活用中 (サポートサイトなど)
  - E2E 用や雑な検証・結合・開発・営業デモ用だと VM 1 台 + Docker で工夫で十分

---

## よくやる構成例: AWS で RDS をベースに無難にコンテナで組む

![bg left:33% contain](images/aws_rds.svg)

- 基本的に AWS でオーソドックスにコンテナメインで普通の Web サービスを組むとこれに近い構成になる
  - (VPC などは省略してある)
- コードプロジェクトの CI/CD で Amazon ECR に向けて Docker イメージを常にプッシュしておく
  - バージョンの管理などそこで行う
- ElastiCache は Redis などのインメモリ DB としてセッションストア等に使う
- S3 はユーザーがアップロードしたファイルの保存などに使う
- 基本的に HTTPS 証明書は AWS に自動で取得＆更新させる
  - 取得した瞬間にホスト名宛に攻撃が来るので注意

---

## よくやる構成例: 全部 VM に入れて組む

![bg left:33% contain](images/one_vm.svg)

- VM 内にいろんなものを押し込む構成
  - Amazon Lightsail をよく使っている
  - リバースプロキシ (caddy) や DB は全体で 1 つを共有することがある
  - VM としてのセキュリティ確保は必須
    - ファイアウォール（ポート制限）、OS パッチアップデートの自動化、ログの保全
- 全体的には Ansible で構成管理
  - 一つ一つのアプリの塊は docker-compose で管理
  - 1 回定番作ればあとはそれを使いまわせばいい
- Caddy で HTTPS を受信＆各アプリにリバースプロキシ
  - 関連記事: [Let's Encrypt で HTTPS を終端させたいだけなら Nginx より Caddy を使うと楽だった件](https://qiita.com/ssc-ksaitou/items/ee0cda84dcf358a2b5eb)
  - 証明書も Let's Encrypt から自動取得
    - 取得した瞬間に[ホスト名宛に攻撃が来る](https://crt.sh/)ので注意（セットアップ中の WordPress など特に要注意）

<!-- - 考慮点
  - Lightsail の FW <=> Caddy <=> Docker (WordPress / MySQL) -->

---

## よくやる構成例: AWS Lambda をところどころで使ってみる

![bg left:33% contain](images/aws_lambda.svg)

- 本体サービスと外部連携サービスする際にちょっとした変換などの処理を挟みたい場合に AWS Lambda を使う
- 最近の AWS Lambda は[無料で `https://` 付きの URL で起動できる](https://aws.amazon.com/jp/blogs/aws/announcing-aws-lambda-function-urls-built-in-https-endpoints-for-single-function-microservices/)のでそこに電文を送り込む
- その他、軽い処理をなんでも AWS Lambda として実装する
- 別の FaaS を使ってもいいと思います
  - データ保存がない系だとこういう構成をとりやすくなる
- 関連記事
  - [最近 Hono で外部連携用の AWS Lambda 書いています](https://qiita.com/ssc-ksaitou/items/4a1a1e437d4139b32d19)
  - [AWS Lambda + Hono で外部 REST API サーバ用のプロキシサーバを書く](https://qiita.com/ssc-ksaitou/items/6815741104185a55e55d)

---

## ほか注目している構成の方法

- [Google Cloud Run](https://cloud.google.com/run?hl=ja) ベースでの構成
  - Docker コンテナをサーバレスで動かすサービス ([適しているサービス](https://cloud.google.com/run/docs/fit-for-run?hl=ja))
  - リクエスト数ベースでのコンテナの起動 ([参考](https://cloud.google.com/run/docs/resource-model?hl=ja))
    - 例: 80 同時リクエストごとに 1 コンテナを立ち上げる
    - 全く使われていない場合、インフラ費用をゼロ円にすることも
      - DB も利用量に応じて `安価な値段〜` にしようとした場合、同社 Cloud SQL では対応できないので、NoSQL (Firebase 等) や DBaaS を使うしかない
  - バッチサービスなどの定時起動もできる
  - 参考: [Cloud Run で作るサーバーレス アーキテクチャ 30 連発 - これのときはこう！](https://speakerdeck.com/googlecloudjapan/cloud-run-dezuo-rusabaresu-akitekutiya-30-lian-fa-korenotokihakou-836ef9eb-8f2a-4b87-afef-dfab362b33c3)
  - AWS よりサービスのスケールについて放置できる面も多く、インフラ費用は安いのではないか？
- 将来的には各ベンダーから自由度の高いコンテナベースシステム＋自由にスケールできる RDBMS の鉄板的な構成は出てくるはず
  - ローカルから本番までベンダーロックフリーで同一構成を保って性能の上限や耐障害性を考えることなく安価に開発していきたい
- **常時、引き出しは持っておこう**

---

## 構成にかかわらず、基本的に最初期から IaC (Infrastructure as Code) を維持してくれ

- IaC = **インフラ定義はコードで管理してくれ**
  - IaC は後から導入しようとすると**悪夢のような難しさ**
  - インフラ周りは基本的に左の画面の値を右の画面にコピーする的なタスクがたくさんあるが、それをコード化することにより**事故がゼロに近くなる**
  - **テスト環境に入れた変更を安心して本番環境に変更できる** (手順書フリー)
- **クラウドインフラは Terraform (OpenTofu) + Terragrunt でやってる**
  - Terragrunt おすすめ記事: [Terraform だけだとハードモードなので Terragrunt を使おう
    ](https://qiita.com/ssc-ksaitou/items/c3eedd46a5eb04d731cc)
  - 何でもかんでも Terraform 定義として管理する必要なし
    - 管理しない例: 所与のものとして手作業で管理するもの（取得したドメイン、作成した DNS ゾーン）、大手クラウド以外の SaaS サービス構成
  - AWS CDK や Pulumi を使うとインフラ定義を TypeScript で書けたり自由度が高い
    - が、Terraform 程度の不自由さが引き継ぎを容易にする
    - プログラミングが生理的に駄目という人種も巻き込める
- **OS (Linux) 上の設定は Ansible でやってる**
  - ほぼ定番だが速度が遅いので数年内に代替ツール台頭はありえる
- **「構成をどのように維持・管理しているか」はテストに出ます！**

---

<!--
footer: "モバイルアプリを考える"
-->

## 「ところで、モバイルアプリ (スマホアプリ) は作るかい？」

- モバイルアプリ、どうしても作らないといけないですかね？
  - Web にバックエンドでお腹いっぱいなんですが……
- Web の**レスポンシブデザイン（画面幅でモバイルレイアウトになるやつ）だけじゃ駄目か？**
  - 最近はダークモードに対応するサイトも増えている
- **[PWA](https://developer.mozilla.org/ja/docs/Web/Progressive_web_apps) (Progressive Web Apps) で逃げられないか？**
  - ホーム画面アイコン OK、オフライン動作 OK、**プッシュ通知 OK**
    - [プッシュ通知は iOS 最近対応, Android は以前から対応済](https://caniuse.com/push-api)
- それでも、「どうしても作る」っていうなら……
- **マルチプラットフォーム対応**のフレームワークを使うか？
  - [React Native](https://reactnative.dev/) (or [Expo](https://expo.dev/)) (JavaScript), [Flutter](https://flutter.dev/) (Dart), [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) (Kotlin)
  - (Expo 以外) いずれもネイティブ依存部分は上手く切り出せるようになっている
- アプリに Web 画面を埋め込んで **(WebView で) お茶を濁せないか？**
  - ストアに公開する際は Apple の [Minimum Functionality](https://zenn.dev/nekoniki/articles/f9d4a8e009603a) に注意
- サーバ側の処理はどうするか？
  - Web 画面(SPA)で立てている API サーバを流用するのが一番カンタン

---

## ストアにアプリをリリースするのは大変という話

- ドメインを取得して自由に公開できる Web サービスと違って、モバイルアプリなどストアリリースが絡むものには **特有の制限があることを理解する**
- ストアの種類と開発者ライセンスの料金
  - [Apple App Store (Apple Developer Program)](https://developer.apple.com/jp/support/compare-memberships/): **$99/年** (高い!!)
  - [Google Play Store (Google Play Console)](https://play.google.com/console/about/publishingoverview/): $25 (1 回のみ)
  - [Microsoft Teams Store (Azure App Registration)](https://learn.microsoft.com/ja-jp/microsoftteams/platform/concepts/deploy-and-publish/appsource/publish): 特に無し
- リリースまで: 実装＆バンドル → **証明書による署名 → アプリの紹介概要作成 → 審査 → リリース**
- **審査が重い**
  - 審査結果が即座に出るわけではない
  - 担当者ガチャあり
  - 定期再審査もあり
  - 審査用のテストアカウント・テストデータ・テスト手順を本番環境に用意する必要がある
    - 国外（米国・インドなど）から審査されると思って OK
  - 審査を回避したければ独自（サイドカーなど）で配布するしかない
- 審査が通らない原因
  - ストアポリシー違反
    - 頻繁にストアポリシーが変わるので追従が大変
  - 新規権限（GPS など）を要求している（要求する合理性がないと判断されている）

---

## モバイルアプリの難しい点

- 一度配布したら、もう回収できない
  - Web アプリと違い、**古いアプリをインストールした端末は残り続ける**
  - サーバから動作下限バージョンを取得し、**アプリ自身にバージョンアップを促す機能を付ける**
- テレメトリの貧弱さ
  - バグやアプリクラッシュ時の環境・**状況把握が非常に困難**
- **機種や OS のバラエティの多さ**
  - Android の機種多すぎ
    - うちは Xperia の謎の不具合が多い……
  - どの OS のバージョンをサポートするか？
- **新機能、iOS/Android どっちに優先的に対応する？**
  - [2024-08 現在、日本は iOS:60%, Android:40% しかし両者は狭まりつつある](https://gs.statcounter.com/os-market-share/mobile/japan)
    - しかし法人端末だと iOS が体感 9 割近いシェアのような気がする
    - [タブレット端末は、日本は iPad: 72%, Android: 26%](https://gs.statcounter.com/os-market-share/tablet/japan)
      - Android タブレットは飲食注文・各種設備の端末にいる印象
  - **日本の B2B においては iOS と結論してもよさそうではある**

---

<!--
footer: "そもそもサービスを自分で作る必要ある？"
-->

## ……そもそも Web サービスを自分で実装する必要ある？

- **既製品をそのまま使う** / ちょっと**カスタマイズする**（独自プラグインなど）
  - WordPress: プロダクトページ、サポートサイト
  - Redmine: 簡単な課題などの登録であれば項目をカスタマイズ＆REST API も完備
  - Jenkins: ジョブの実行基盤として
- **半製品系**
  - Hasura: GraphQL サーバを DB から自動生成
  - ヘッドレス CMS: サイトのコンテンツだけを管理する
- ちょっと告知するサイトが欲しいだけ
  - Google Sites: とりあえずこれでサポートサイトを構築する
- 問い合わせ・入力フォーム
  - Google Forms など

---

<!--
footer: "まとめ"
-->

## 技術編のまとめ

- サービスを作る際には
  - コンセプトや **MVP を考えよう**
  - DB と画面・AP(I) とバッチがあればサービスは成立します
- **いろんなサービス構成のパターン**がある
  - DB がメインキーになることが多い
  - OS やサーバを手放すと楽になれることが多い
  - 当座を安く済ませたいなら VM 一台に色々入れてしまってもいい
  - AWS とか大手で組んでしまうのは安牌といえば安牌
- モバイルアプリは**ストアリリースが難しい**
- **ちょっとずつ進化できるようにしよう**
  - Docker は広くリスクヘッジになるのでおすすめ
  - IaC でインフラを管理しよう
- もちろんここに書いてない色んなことを考えて作りましょう

---

<!--
_class: primary_section
header: "運用編 〜 サービスを運用する"
footer: ""
-->

# 運用編 〜 <br>サービスを運用する

サービスを運用していく上で期待されるポイントを整理していきます

---

<!--
footer: "課題収集しよう"
-->

## 課題を収集する

- 普通に集団でサービス運用をやっていく上で、メンバーが次に何をすべきなのか、どういう課題があるのか、Wiki など含めて**オンラインで情報を共有・把握する仕組み・ハブが必要**
- **[ITS（イシュートラッキングシステム）](https://ja.wikipedia.org/wiki/%E8%AA%B2%E9%A1%8C%E7%AE%A1%E7%90%86%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0)はプロジェクトの最初から最後まで確保してほしい**
  - **ソースコード管理と一体タイプ**: [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/), [GitHub Issues](https://docs.github.com/ja/issues)
    - 正直ソースコード管理と一体化していると**幅広い人を呼びづらい**し管理単位もそっちに引っ張られるので **[逆コンウェイ](https://medium.com/i35-267/%E3%82%B3%E3%83%B3%E3%82%A6%E3%82%A7%E3%82%A4%E3%81%AE%E6%B3%95%E5%89%87%E3%81%A8%E9%80%86%E3%82%B3%E3%83%B3%E3%82%A6%E3%82%A7%E3%82%A4%E3%81%AE%E6%B3%95%E5%89%87%E3%81%8B%E3%82%89%E7%B5%84%E7%B9%94%E6%A7%8B%E9%80%A0%E3%82%92%E8%80%83%E3%81%88%E3%82%8B-bf3f32ebb022)で個人的に好きではない**
    - コードプロジェクト管理だけに特化するならアリかも
  - **SaaS タイプ**: [Jira](https://www.atlassian.com/ja/software/jira), [Backlog](https://backlog.com/)
    - 自前で運用が要らないのが一番のメリット
    - 人が増えるほど**お値段が高い**＆**カスタマイズできない**＆データが人質に取られるのが辛い
  - **自前で立てる（セルフホスト）**: [Redmine](https://www.redmine.org/)
    - 何人呼んでもサーバ代＋管理工数だけで**お安い**
    - カスタマイズ性が高い
      - **最悪プラグインで拡張すればどうにかなる**
    - **REST API で好きに外部連携させられる**
    - 性能の管理とバージョンアップなどの面倒が最大の難点
- 基本的にプロジェクトメンバーは幅広く参加させておいて、**情報の中心的ハブにする**
  - マネージャー、営業、エンジニア、サポートチーム など
  - **チャットはフロー、ITS はストックと使い分ける**

---

<!--
footer: "対顧客の機能の充実"
-->

## 顧客にサポートを提供する

- 「**問い合わせフォームが欲しい**」と言われたら…
  - Google Forms: **初手としてはアリだがカスタマイズ性やファイル添付で問題を抱えることになる**
    - Microsoft Forms も同じ
  - WordPress: [WordPress CF7](https://ja.wordpress.org/plugins/contact-form-7/) など
    - 問い合わせの内容を Redmine に登録して、複数担当者で分担して回答している
- 「**サービスのサポートサイトが欲しい**」と言われたら……
  - サービス自体に作り付ける: コンテンツ更新のたびにサービスリリースを許容できるならどうぞ
  - **Google Sites**: WYSIWYG で簡単にサイト作れる
    - **初手としてアリだがブランディングとしてイマイチ**
  - **WordPress**: CMS として定番中の定番
    - 自由度高い。会員企業様限定コンテンツなども展開できる。
    - **脆弱性を生みやすい・攻撃されやすい**ので、セキュリティ対策と定期アップグレードはしっかりすること
    - 本当に立てたくなかったけど、結局立てることになった
  - うちでは営業・サポート・技術でコンテンツを更新している
- サポート受付・回答用の**メールアドレスの確保**
  - サポートサイトからの自動返信、パートナー及びその他関係者からの手動返信
    - サービスドメイン or サブドメイン (`@support.example.com`)でメール送信できる必要がある
    - 人間の担当者用には Google Workspace で Gmail 使ってる

---

## 顧客に料金を請求する

- **課金体系によって全然難度違う**
- シンプルな課金体系なら営業側で手動管理
  - ユーザ側で追加可能なユーザ数・ストレージなど**契約した利用上限を管理をシステムに登録＆営業は契約分を請求**
- 複雑な課金体系なら**本体＆独自の請求管理システムで管理**
  - システム本体側で**日次バッチでユーザーごとの利用量を集計**
    - 課金体系の粒度に合わせた使用量を集計していく
    - ユーザーが大手だと部門別に使用量を算出してほしいニーズはある
    - ユーザー自身が管理画面で自テナントの使用量を確認できるとよい
  - → **請求管理システムに利用量をシステム間連携し、最終的な課金額を算出**
    - → 社内原価管理システム・請求 SaaS などに連携
    - 値引き・お試し期間・経由代理店・単価の違いなど本体で持ちたくない情報をここで吸い取る
- 労力に見合わない課金を深追いしない
  - 月またぎなどで出てきてしまった微小な課金はユーザーにオマケとして提供する
- 請求は争いの種になるので**後日手で検証できるぐらいシンプルにしたほうがいいと思います**
- (決済手段については扱いません)

---

<!--
footer: "サービスの改善"
-->

## 継続的にサービスをリリース・改善していく

- サービスの**新規機能開発はなるべく小さいステップに分割する**
  - いきなり大変更を行わない（**ホップ・ステップ・ジャンプ程度には分解 → チケット分担**）
  - すぐに必要のない機能は後回しでよい
  - **将来のことを考えすぎない**　わからんものはわからん　開き直る
- **機能拡張がスムーズにいくかは普段の行い（内部品質）次第である**
  - **リファクタリング**はしているか？ミスを減らすために型システムなど適切に使えているか？
  - **ユニットテストやコードレビュー** はきちんと行われているか？
  - **リグレッションテスト（回帰試験）** の自動化は行えているか？
  - きちんと**業務上の語彙や意味がコードと一体化**されているか？
- 万が一リリースが失敗だった場合に、**リリースを差し戻せない変更はなるべく避ける**
  - カラムの削除・テーブルの削除などは差し戻せない典型例　**今すぐそれ消す必要ある？**
    - しばらく経って本当に不要と確信できる時期に消せば良い
  - 新旧移行があるものは、基本的に新旧両方を同時に動かして、パチっと切り替えるのが基本
- リリース時の**ダウンタイム避けは七難隠す**
  - Blue/Green デプロイなど
  - リリース時にダウンタイムがないと、顧客に告知しなくて済むなど色々と良いことが多い

---

## サービスを「計測 ⏱️」する

- **メトリクス（メモリ使用量など数値情報の時系列データ）を取る**
  - APM ツールなどを使ってサービスの性能を計測
  - リクエスト・バッチなど処理単位でかかった時間・結果を計測＆出力
- **ログを取る**
  - **長期保存用**: S3 などにサービスとして定めた期間全てのログを圧縮・保存する
  - **短期利用用**: 数カ月分のログを検索に優れたサービス上で保存・活用する
    - 直近の事象についてのトラブルシューティングに用いる
    - CloudWatch Logs, Elasticsearch (OpenSearch) などに入れる
  - **ログを出す側の実装**としては以下のようなものはあってほしい
    - `{INFO: X処理スタート, traceid=XYZA, oid=999, uid=9999[, success, elapsed=0.1234]}`
    - `{ERR: Xに失敗した, traceid=XYZB, oid=999, uid=9999, err=XXXException...}`
    - **外部連携や特殊操作がある場合は特に記録してほしい**（集計したい）
    - **今どきは全てのログは全て JSON でいい**

---

## 計測データを活用する

- **ログや計測データは、とにかくホストマシン上などにファイルとして留めておかない**
  - ファイル保存は際限なくディスクを埋める、ホスト接続不可とともに利用不可に
  - (例) ログはすべて [fluentbit](https://fluentbit.io/) などを使って信用できる場所に即時配送する
- ログを定期的に解析するツールを作って定期的にデータベースに解析結果を保存しておく
  - ビジュアライズとして [Grafana](https://grafana.com/ja/) や BI ツール([Amazon QuickSight](https://aws.amazon.com/jp/quicksight/) など)を使って可視化する
- 最近ホットなキーワード
  - Observability, OpenTelemetry

---

## サービスを「監視 👁️」する

- サービスを監視する
  - **外系監視**: 外部サイトからの HTTP リクエストを定期的に投げて応答を確認する
  - **内系監視**: サービス内部の状態を監視する（メトリクスを監視する）
    - プロセスの生存確認、ディスク容量、メモリ使用量、CPU 使用率、ネットワークの遅延、サービスの応答時間
    - 基本的には APM ツールや Zabbix 等の監視ツールで監視する
- **アラートを定義する**
  - メール通知・チャット（Teams）通知・SMS 通知など
    - Teams は通知用のメールアドレスをチャネルを右クリックすれば取得可能
      - なんか気難しくてメールが弾かれていることが多いのが微妙
  - あまりにも恒常的だと狼少年になるので、**アラートは出ないことを基本とする**
  - 「必要そう」でアラートを出しておかない　今対処しないものはアラートではない
- **緊急時のアクション**を定義しておく
  - サービスによっては人力で対応してくれる場合もある（サーバ再起動コマンドを打つなど）
  - AWS であればあらかじめ用意したプレイブックを実行するサービスもあったりする([SSM Automation Playbook](https://aws.amazon.com/jp/about-aws/whats-new/2019/11/automate-your-operational-playbooks-with-aws-systems-manager/))
- 休日は誰が監視しているの？！ページャ（ポケベル）は誰が持っているのか？！誰が対応するの？！

---

## 脆弱性 (vulnerability) に対処する

- **脆弱性をコントロールする対象を把握しておく**
  - 既製品: アプライアンス、OS、サービス、フレームワーク、ライブラリ
  - 自作のコード: セキュアコーディングなどのコードの内部品質、脆弱性診断等で対処
  - 外部に接しないものについては優先度は低い
- 脆弱性をコントロールする**対象・種類をあらかじめ減らしておくのが最善**
  - サーバレスやマネージドサービスを使う、**とにかく自分が管理する OS を持たない**
    - VM でサービスを管理していると OS のせいで管理工数が上がる
  - 使う技術スタックやプロダクトの**種類を抑える**
- **定期対処**
  - 定期的（**半年以内目安**）に **↑ のコントロール対象を中心に最新の安定版にあげていく**
    - 放置すると: サポートが切れる、いつか上げられなくなる、いざ上げる時の改変が大きすぎて予算捻出ができない、依存製品終了の大イベントに対する改変予算を積めない etc.
  - リグレッションテスト（回帰試験）の実施が必須
    - ユニットテスト、E2E テストなどの**自動テストの充実で回帰試験コストが大きく変わる**
  - ここのコストを捻出できないサービスは半分死んでる兆候
- **緊急対処**
  - 緊急対処が必要かどうかアンテナを張っておく ([CVE](https://cve.mitre.org/), [IPA(JVN)](https://www.ipa.go.jp/security/vuln/index.html))
  - Workaround (WA=**その場しのぎ**) と **恒久対処** の 2 プランを用意して対処する
    - Web サービスの場合、攻撃リクエストのパターンを WAF で緊急回避できる場合もある

---

## 脆弱性診断を依頼する

- 新機能のリリース前や定期的に第三者に脆弱性の検出を依頼することもできる
  - むしろ顧客から証跡として要求される場面もあるので**実質必須といってもいい**
  - **セキュリティスキャナの実行代行（安価）** と **人の手の試験も入れるもの（中価）** と **コード評価含めたもの（高価）** がある
    - スキャナ実行代行はアリバイ工作にしかならないからやらないほうがいいかな……
- 脆弱性診断実施までの流れ
  - 発注打診 → 対象サービスについてのヒアリングシートへ回答 → 依頼 → 診断日までにテストデータ準備 → 診断実施 （**診断中は環境は変更不可＝脆弱性診断用に一つ環境を空けておく必要がある**） → 診断速報 → 診断レポート(1 回目)
  - → 修正する診断を判断 → 診断の指摘修正 → 再診断 (or 最終診断)
    - 顕在化しない脆弱性、誤検出、対処の費用対効果が薄すぎるなら**対処しない判断もある**
  - → 最終診断レポートの受領
- よくある指摘
  - [CVSS](https://www.ipa.go.jp/security/vuln/scap/cvss.html), [OWASP Top Ten](https://owasp.org/www-project-top-ten/) などのセキュリティリスクに準拠した指摘
  - サーバ: サーバレスポンスヘッダにサーババージョンが残ってる、CSP などの推奨ヘッダがない、TLS の古い暗号化方式に対応してしまっている、起動できてはいけない処理が起動できる など
  - スマホアプリ: セキュアな情報（緯度経度・顔写真）が暗号化されずストレージに保存されている、ATS (HTTP 通信を認める設定)が入っている など

---

<!--
footer: "チームの内政"
-->

## メンバーを教育する

- あらゆる開発・保守運用**タスクを棚卸ししておく**
  - 概要・利用している技術スタック・現在の識者をはっきりさせておく
  - 各タスクが**少なくとも 3 人の識者によってカバーされるようにコントロールする**
    - 識者が少ない業務があると、休みを取れない業務のヌシが出来上がる
    - 特に知識が豊富な新規実装者にタスクが閉じてしまうケースが多く、新しい機能を追加する際に対応できるプロフェッショナルがいなくて困るケースになりやすい
- **輪読会**
  - 新人教育を兼ねて様々なテーマの本を日次で読む
  - 対象とする技術スタックについての本やマニュアルを希望者で週次で読む
- **コードレビュー**を活用する
  - レビュー目的ではなく**コードを強制的に共有して把握させる意味合いでコードレビューを用いる**
- よく参照するものについては Wiki に記録してもらう
  - **一部の人しか Wiki を更新しなくなりがち** なので、全員に更新させる
  - Markdown の巧拙が出やすいので、Markdown の書き方を教育しておく
- サポートチームからの**質疑応答を消化してもらう** / 敢えて**新人にドキュメントを書いてもらう**
  - 仕様理解の促進に役立つ

---

## 定期的な会議体（週次）を持つ

- ITS の**チケット（課題一覧）の棚卸し**を行う
  - 担当がいない・放置チケット・各タスクの進捗の確認
  - 自由に話し合う空気を作る
- 定期的に**各種監視・数値等をあらためて確認させる**
  - APM ツールなどで計測しているメトリクス、バッチの実行時間など
    - 短い期間だけで見ると分からない傾向があるので、長い期間（数ヶ月以上）でグラフを見る
    - 傾向について仮説を洗い出し＆課題としてチケットに残す
      - 曜日・時間帯・休日とアクティブユーザ数の関係
      - ユーザ数に直接連動（マシン負荷）、ユーザ数が時間で積分されるもの（ディスク使用量）
      - 計算量の異常増加（`O(N^2)`的なパフォーマンス課題の暗示・DB インデックスの不在）
  - **クラウドや SaaS サービスの請求費用・使用量の分析（毎月頭）**
    - クラウド上はタグやプロジェクト別で集計できるようにあらかじめ設定しておく
  - システム管理者の操作履歴も見返す
  - 確認結果は証跡としてチケットに残しておく
- アカウントの棚卸しを行う
- 振り返りもできると良い

---

## チームのモチベーション維持

- **最大の問題かもしれない**
- **サービス開発は終わりがない、区切りがない**、永遠に延々と課題との格闘が続く
  - 請負開発、現場業だとプロジェクトが終わるという区切りがあるが、サービス開発は永遠に続く
    - 流行っていないサービスでもユーザの問い合わせ・脆弱性対処・定期タスクがある
  - 郵便配達員が終わりのない配達に嫌気が指して郵便物を捨てる事件を思い出す
  - 機能開発で区切ればと思うかもしれないが、いろんな企画とサービスとプロジェクトが同時並行して動いているので本当に区切りがない
- 裁量がなくてつまらない問題
  - 機能の専任担当をなるべく設けて、主体的に考えるようにしてもらう
- [インポスター症候群](https://ja.wikipedia.org/wiki/%E3%82%A4%E3%83%B3%E3%83%9D%E3%82%B9%E3%82%BF%E3%83%BC%E7%97%87%E5%80%99%E7%BE%A4) (自分なんて役に立っているのか……と思うこと)
  - 人のいいところはこまめに褒めよう！
- マルチタスクで何にも集中できない
  - あまり重要じゃない仕事は部として畳んでいく
- 脱マンネリ
  - 他の開発者が集まる勉強会で刺激を得る
- **結局、どうすればいいんですか？**

---

<!--
footer: "まとめ"
-->

## 運用編のまとめ

- サービスの運用するために必要な道具はいろいろある
  - ITS(課題管理ツール)
  - 問い合わせフォーム・サポートサイト・問い合わせ対応
  - 請求管理
- **サービスの運用体制を当たり前まで持って行くのは結構大変**
- サービス運用に欠けているピースについて自覚的になる
  - フィードバックを取得する手段を増やす
    - 脆弱性診断や顧客の問い合わせ
    - システムのメトリクスやログ
- プロジェクト・チーム運営の問題に自覚的になる

---

<!--
_class: primary_section
header: "運営編 〜 いろんな人達からの期待に応える"
footer: ""
-->

# 運営編 〜 <br>いろんな人達からの期待に応える

サービスを運営していく上で、チームや社内外からの期待に応えるためのポイントを整理していきます

---

<!--
footer: "チームからの期待"
-->

## チームからの期待

- 営業「営業用の**デモ環境**が欲しい。お客さんに披露したいので、**先行して新機能を追加してほしい**」
  - **毎回よくある。** 正式な環境じゃなくてもいいので、VM 適当環境でもあると吉。
- 開発「開発用のテスト環境・テスト用の外部 SaaS のアカウントが欲しい」
  - **あらゆる外部連携系について試験用アカウントの確保は必ず行うこと。**
- 開発「画面開発したいので、サーバのインタフェース仕様だけ早くリリースしてほしい」
  - **自分でボールを持ち続けないように立ち回ろう。**
- 開発「こういうツールがあると助かるから欲しい」
  - **まともなサービス運用には大量の補助ツールが必要なのが真理。**
- 偉い人「**`${今までやったことのない技術がここに入ります}` を導入したい**」
  - プッシュ通知、GPS、地図連動、住所、動画、チャット、デバイス連携、OCR、**AI (?!)** など
  - **無茶振りすごい**けれども、対応には普段からアンテナ伸ばしておくほかない。
    - 「使えて当たり前」感と「実際の開発コスト」は関係しないことを痛感する。
    - 企画で話自体が流れることがあるので、必要以上に早期段階で追いかけない。
- **無理して対応したものはきちんと振り返って効果があったかは共有したほうがいい。**

---

<!--
footer: "社内からの期待"
-->

## 社内からの期待

- 主な期待
  - **コンプライアンスリスクの低減**
    - 細かいことは知りたくはないが、とにかく見えない地雷を抱えていないか？
      - セキュリティ保護してるよね？
    - 認証・コンプライアンス(ISMS や個人情報保護指針など)へきちんと適合しているか？
    - そのサービスの利用規約は自社をきちんと保護してくれているのか？
  - **変更への説明可能性**
    - 新規機能開発の稟議・根拠・想定工数
    - リリース判定に対する説明、安全性の説明
  - **社内システム（原価・請求・勤怠）とのスムーズな連携**
- 変わった期待
  - 評判の向上（受賞）・株価
  - ドッグフーディング（自社サービスを自社で使う）

---

## コンプライアンス 〜 原則的にセキュアでいる姿勢

- 細かいコンプライアンス準拠はさておき、**原則的にセキュアであり続ける姿勢**を貫いたほうが柔軟に対応できる
- 普段からきちんと**運用や対応の全てを文書化しておく**
  - 計画（文書化）→ 実施（文書化）→ 振り返り（文書化）→ （以下無限 PDCA）
    - 難しい対応は複数人でレビュー or 実施は立会複数で行う
  - Redmine に細かくチケットや Wiki を残す習慣づけ （あるいは Git などで管理）
    - 変更履歴も残るし、検索もできる
    - あまり形式にこだわらないほうがいい　とにかく役立たせる＆文書を残すのが最優先
      - 個人の TODO や備忘録として Redmine を活用するぐらいのノリのほうがやりやすい
  - 物理資産・情報資産も一覧化しておく
- **権限は最小権限の原則**に従って、「**個人ごと**」「**最低限のみ**」付与して「**定期的に棚卸し**」する
  - できれば一定期間使わないか、利用可否に回答がなければ自動的に権限を剥奪できるとよい
  - ID は OIDC/SAML で寄せる＆AWS のアクセスキーも SSO のキーで代用するなど
- **認証は知識だけに頼らない**: [多要素認証・パスキー・OIDC・秘密鍵認証 など](https://ksaitou.gitlab.io/2024-02_webauthn_techs/)
- **メールの利用は極力避ける**
  - 頻繁に対話する相手はチャットや ITS を使う　メールを使わなければ誤送信は無い！

---

## コンプライアンス 〜 原則的にセキュアでいる姿勢

- **個人情報はなるべく扱わない**
  - 仕方なく集計等を行う場合は、匿名化（仮名化）を行い、自社のみでの利用に留める
    - 個人 PC に極力ダウンロードさせない
    - 当初の目的外利用しない（個人を本人の意図しない情報処理から保護する）
  - 本番データは取扱が非常に難しいので**テスト用にデータ生成ツールを用意するのはおすすめ**
- **個人の努力より仕組みに重きを置く**
  - 監査ツールも使う (AWS であれば Amazon GuardDuty など)
- **知識の非対称性を安全性の根拠にしない**
  - 攻撃者がコードベースや内情を熟知の上で攻撃する想定でいる
    - NG: 自社独自の暗号化方式を使う、自社独自の認証方式を使う
- **時には攻撃者の気持ちになる**
  - こういったセキュリティセンスがある人が優先してバックエンドサービスやインフラを担当したほうが大事故は少ない
- マルチテナンシーサービスは**顧客ごとのデータ分離性に気をつける**
  - 他の顧客とデータが混ざると最悪
    - できれば[RLS (Row Level Security)](https://zenn.dev/taxin/articles/postgresql-row-level-security-policy)など導入したい…
- 基本的なセンス感は、普段の教育次第かな……

---

## 社内からの期待 〜 その他

- それはともかくとして社でサービスをやっているなら必要なもの
  - **サービス仕様書**
    - サービスの利用開始方法・動作環境・提供範囲・機能・提供条件
  - **利用規約・約款**
    - データを保管・処理する外部クラウドサービスについての顧客への説明
  - **プライバシーポリシー**
- 新機能リリースなどの変更の説明はできるようにしておく
  - 計画・記録を**文書化しておく**
  - **サービスの品質を示す根拠**を集めておく
    - 第三者による品質試験・脆弱性チェック結果
    - 自分達で保持している品質レポート
- 社内システムとの連携
  - Excel や CSV を出す口があると助かることが多いかも
  - 最悪 API やデータベースから必要データを出力するツールを作る

---

<!--
footer: "社外からの期待"
-->

## 社外からの期待

- よくあるもの
  - （大口さん）定例ミーティング
  - **セキュリティチェックシート**
- たまにあるもの
  - 専用環境・専用アドイン
  - 管理画面の接続元 IP アドレス制限
  - 社内統制への準拠
  - ログ出力
  - ユーザの一括登録・更新
  - 課金根拠の詳細説明

---

## 定例ミーティングとは

- 大口のお客様向けに定例ミーティングを行うことがある
  - 30 分ぐらい
- 内容としては以下が主
  - 困っていることなどフィードバック収集
  - 次回のサービスアップデートの内容の事前説明
  - サービスに対して要望をもらう
    - → 持ち帰って検討
- フィードバックをもらえる貴重な場所として活用
  - **ちゃんと対応してエンゲージメントをあげたい**

---

## セキュリティチェックシート 〜 その重責

- 見込みのお客様がサービスのセキュリティなどの特性に対しての質問を Excel シートにまとめて送ってくることがある
  - 「うちのセキュリティ要件を、貴サービスは満たしてますよね？」
- 案外**回答には労力が必要だったりする**
  - 目安: 4 時間(慣れていない場合) 〜 1 時間(慣れてきた場合)
- サービス・会社によっては回答に料金を請求するケースもある
  - （うちは今のところ料金は請求していない）
- あらかじめ想定セキュリティシートに回答しておき、それを参照してもらう手法もある
  - [Assured](https://assured.jp/)
  - 参考
    - [IPA 安全なウェブサイトの作り方 セキュリティチェックシート (xlsx)](https://www.ipa.go.jp/security/vuln/websecurity/ug65p900000196e2-att/000044403.xlsx)
      - [安全なウェブサイトの作り方 (pdf)](https://www.ipa.go.jp/security/vuln/websecurity/ug65p900000196e2-att/000044465.pdf)
    - [総務省 クラウドサービス提供における情報セキュリティガイドライン（第 3 版）](https://www.soumu.go.jp/johotsusintokei/whitepaper/ja/r04/html/nd245620.html)
    - [経産省 サイバーセキュリティ経営ガイドラインと支援ツール](https://www.meti.go.jp/policy/netsecurity/mng_guide.html)
- **大きな企業案件を取るには避けては通れない道**

---

## セキュリティチェックシート 〜 質問の一例

- バックアップについて 保管場所、保持期間、取得頻度、暗号化の有無を教えて下さい。
- 脆弱性の情報収集はどのように行っていますか？
- インシデントへの対応体制および対応時間はどのようになっていますか？
- データセンターはどこにありますか？
- WAF ありますか？IDS（侵入検知）ありますか？アンチウィルスソフトは導入していますか？
- ユーザーの行動、システム管理者の行動について監査を行っていますか？ログはありますか？
- パスワードポリシーは何ですか？弊社基準を満たしていますか？
- 電子政府推奨暗号リストに記載の暗号化アルゴリズムを用いていますか？
- HSTS は有効ですか？TLS に古い暗号使っていませんか？
- 負荷試験してますか？
- セキュリティアーキテクチャのレビューはドメインアーキテクチャチームによって実行および承認されていますか？それともソフトウェアライフサイクルの一環として計画していますか？
- 他部門や会社に確認しないと回答できない系の質問など

---

## セキュリティチェックシート 〜 回答のコツ

- 考えすぎず、**真実を答える**
  - できないことはできない
  - それで失注しても縁がなかっただけ
- 設問の意味が分からない時
  - 英語の意味がわからないなら、さっさと調べよう
    - 日本語訳の意味が分からない場合は英原文を見よう
    - 外資系企業はグローバルで共通したセキュリティシート（英文ママ）を割と投げてきます
  - 言葉が多義・曖昧すぎて分からない場合は設問の見出しを見て、その意味を推測しよう
  - 「設問の意味をこう解釈した」という前提とともに回答を書こう
- 全部準拠しないといけないわけではない
  - 準拠できずとも向こうがサービスの導入に積極的な場合もある
- 大手パブリッククラウドを使っていると回答が楽なケースが多いと思う
- 会社の取得認証が強力なほど設問は減る
  - [SOC2 (Service Organization Control Type 2)](https://assured.jp/column/knowledge-soc2) が最強らしい
- **世間の常識との自社サービスのズレを教えてくれる貴重な機会と捉えよう**

---

## その他顧客からの期待

- **専用環境・専用アドイン**
  - 「うち**専用のサーバ**がほしい」「うち**専用の機能**がほしい」
  - これを SaaS 事業としてやると保守コスト（主に工数）が倍以上になることは自覚する
    - テスト環境＆本番環境をメンテナンスすればよかったのに、そこに他の本番環境を追加していくとどうなるか分かりますよね？
    - サーバの性能も一社だけに振ると勿体ない
  - 会社ごとの専用モジュール差分を野放図に開発に入れると環境構築等が地獄になる
    - ユーザ全体で利益になるような機能に昇華できないか？
    - 最悪ユーザごとに独自機能を ON/OFF するようにできないか？（試験工数は？）
    - 外部サービスとして独立した存在として本体と分離できないか考える
  - 専用環境ぐらいの分離性が欲しいならアーキテクチャ的に解決できないか探る
    - RLS (Row-Level Security) など
  - とはいえ、絶対にやってはいけないわけではないので、**対応できる人員の余りと金次第……**
- **大量のサポートの依頼**
  - 基本的にある程度までサービスが成長すると、日々の問い合わせを自社メンバーだけで捌ききれないので、一次対応は別組織に外注することになる

---

## その他 〜 続き

- **社内統制への準拠**
  - 「認証を社内の SSO に対応してほしい」
  - 「モバイルアプリを社内 MAM に乗せるために生アプリを提供してほしい」
  - 「管理画面をこの IP アドレスからしか繋げられないようにしてほしい」
- **ログ出力＆請求の詳細確認**
  - 「先月の使用ユーザ数絶対間違いだと思う」
  - 「この日に登録しているはずなのに何故か反映されていなかった」
  - 過去データベースを問い合わせ用に復元しないと回答できない内容も割とあったりする
    - これについては DB サイズが巨大になったら回答できなくなりそう
- **ユーザの一括登録・更新**
  - 「新規登録するので、部門と社員（数百人〜数千人）を一括登録してほしい」
  - 「組織改編に合わせて一気に社員情報を更新してほしい」
  - CSV 一括登録機能を用意しておいても、吸収できない特殊大口要件は来る
    - 専用フォーマット・専用ツール・専用の運用を用意してある
    - DB の直接編集は避け、API から間接的に操作するように作る（画面操作できないことはやらない）
- できない・割に合わないは、できないで OK
  - 大口顧客や顧客が少ない時期に冷静に判断できるかな？
  - **それでも取りに行った分については費用対効果測定をやってほしい**

---

<!--
footer: "まとめ"
-->

## 運営編のまとめ

- サービスに対して色んな利害関係者が ~~無茶~~ 要望を言ってくる
  - チーム、社内、社外
- **普段からセキュリティに関心を持ったり、対応や証跡を記録しておこう**
- サービスへの要望は**後出しジャンケンが本質**ぐらいに思おう
  - 冷静に対応を考えよう

---

<!--
_class: primary_section
header: "最終章 〜 そしてサービスが終了する"
footer: ""
-->

# 最終章 〜 <br>そしてサービスが終了する

---

## どうしてサービスが終了するか？

- **稼げていない（利益が少ない）から**
- なぜ: **売上が低い**
  - ユーザが少ない
  - ユーザ単価が安い
- なぜ: **費用が高い**
  - インフラコストが高い
  - 人件費が高い
  - 結局維持費が高いとロングテール戦略はできない
- これらを**改善しようにも、改修費用すら出せない**

<span class="sub">（これまでのシステム開発費は埋没費用としてさっさと撤退したほうがいいが……）</span>

---

## サービスを終了するには？

- **新規ユーザー受付停止**
  - サインアップフォームなど停止
  - コーポレートサイトからも撤去
- サービス**終了時期の設定＆顧客への周知**
  - 約款であらかじめ周知に必要な事前期間を定めておく（半年前など）
- 顧客**データエクスポート措置の提供**
  - 機能開発が出来ない場合は手作業で提供することも検討
- （期日が来たら）**サービスの停止**
- （特に反応がなければ）**サービス撤去＆データ消去**
  - サービスに関連する全リソースは把握しておく
- **アプリなどをストアから撤去**

**これにて本サービスは終了です。お疲れ様でした。**

---

<!--
_class: primary_section
header: "まとめ"
footer: ""
-->

# 本日のまとめ

---

## 本日のまとめ

- **技術チーム**側から見た**Web サービスの立ち上げ〜運用・運営に関わる幅広いフルスタックな話**をしました。
- 本日の内容
  - サービスの立ち上げ
    - いろんなアーキテクチャ・技術選定がある
  - サービスの運用
    - 副次的なタスクが無数にある
  - サービスの運営
    - いろんな人達からの期待に応えるのは大変
  - サービスの終了
    - 結局のところ儲からないと終わり
- **「普通のサービス」をやるだけでも結構労力がかかる**

## 参考図書

- トリリオンゲーム
- 王様達のヴァイキング

---

<!--
footer: "質疑応答"
-->

## 質疑応答

- **Q. DevOps をやるべきか？**
  - 今回の内容はまさに全員で開発から運用まで思いを馳せ、その間に強い境界線を引かない DevOps スタイルそのものです
    - 技術的なロードマップは[有名なものがある](https://roadmap.sh/devops)ので参考にするといいと思います
  - 一般的な Web サービスを運用していくのであれば DevOps は実践したほうがいいと考えます
    - 開発者は開発にのみ能力をスケールさせなければ、勿体ないという発想もあると思います。しかし運用を一通り回さないと見えないものが多いはずです。
  - 一方、公共・金融系など DevOps スタイルが求められておらず、従来からも実践されていない分野で強く推奨はされないと思います
- **Q. DevOps は何を使うべきか？**
  - 使うソフトウェアについてはメンテナンスが活発で利用者が多い OSS を好んでいます
    - インターネット上に同じトラブルに遭遇した人間が見つかりやすいのは大きな強みです
    - 逆にあまり必要としていないものは商用サポートの類です。今まで幸いにも商用サポートが必要になることはありませんでした。
  - メンテナンスが活発なソフトウェアをこまめにアップデートしながら、高速で回帰試験を回すというスタイルになります
  - 運用スタイルは SCRUM など色々あるので（詳しくないので）割愛します

---

<!--
header: ""
footer: ""
-->

# おわり
