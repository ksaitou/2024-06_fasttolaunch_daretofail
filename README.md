# 2024-09 「何度でもサービスを作ろう」

- スライド: https://ksaitou.gitlab.io/2024-06_fasttolaunch_daretofail/
- リポジトリ: https://gitlab.com/ksaitou/2024-06_fasttolaunch_daretofail

```
タイトル：「何度でもサービスを作ろう」
開催日時：2024年9月24日（火）13:30-15:00
概要：
小さなサービスから大きなサービスまで、様々な期待を乗り越えてWebサービスを作るためのノウハウを共有します。

講座内容：
・サービスの立ち上げ方
・「早い安い美味い」は出来るのか？
・サービスの保守を考える
・組織からの期待への対処方法
・組織外からの期待への対処方法

対象者：
・Webサービス開発者
・サービス立ち上げ運用に関心があるエンジニア
・Webサービスのプロジェクトに携わる人
```

## スライドの作り方

### スライドの開発

```
# 依存性取得
$ pnpm install

# サーバ: http://localhost:8080
$ pnpm start

# HTMLで出力
$ pnpm build
```

### 参考

- marp (プレゼンツール) - https://github.com/marp-team/marp-cli
  - https://marpit.marp.app/markdown
